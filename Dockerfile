FROM maven:3.5.3-jdk-8
ADD . .
RUN mvn compile
ENTRYPOINT [ "mvn", "test"]