# Assignment 3 - ORM

### Student 

Francesco Ribaudo 807847

## Overview

The project implement a simple system for manage a company. It is possible to execute CRUD and search operations on a set of entities that inchttps://gitlab.com/Ribbo27/pro-svi-so-assignment-3/blob/master/REPORT.mdlude **employees**, **projects**, **offices** and **cities**.

 For this project I used **Spring Data JPA**, which implements the JPA specification via **Hibernate**. Moreover, the system connecting to a **MySQL** database via **MySQL JDBC connector** to store entities and their relationship. All projects dependencies are managed by **Maven**.

The whole project source code is available on: https://gitlab.com/Ribbo27/pro-svi-so-assignment-3.git 

## ER Model

Both entities and relationships are shown in the following diagram.	

![](/home/ribbo/Scrivania/Magistrale/ProSviSw/Progetti/Assignment3/ERDiagram.jpg)

Entities are related to each other:

- Between **Offices** and **Cities** there is a unidirectional `many-to-one` relationship because in one city there may be several offices.
- Between **Offices** and **Employees** there is a bidirectional `one-to-many` relationship because different employees can work in the same office. 
- Between **Employees** and **Cities** there is a unidirectional `many-to-one` relationship because different employees can live in the same city.
- Between **Employees** and **Projects** there is a bidirectional `many-to-many` relationship different employees can participate in different projects. The owner of the relationship is the Employee entity.
- Finally, **Projects** entity has a `one-to-many` self-relationship because a project can have one or more sub-projects, and sub-projects can have at most one parent-project.

## Classes

The base package for this project is `com.ribbo.orm`

- Package `entities` contains all JPA classes. Each class is a JPA Entity and is mapped into MySQL database.

  - `Address` class represents an **embeddable** entity that represents the address of both employees and offices.
  - Each entities has an override method `toString()`.
- Package `repositories` contains all the repository classes for managing entities, with which we can store and retrieve data from the database. An abstract class called `Repository` has been created that manages the entity manager and is extended and implemented by all the other specific repositories.
  - Each repository implements all methods defined in `Repository` class, which allow to perform CRUD and search operations within a database. 
- Package `services` contains all classes for managing entities at an higher level than `repositories`. The Service classes use the Repositories to save and retrieve data from the database and in addition perform logical operations, such as detaching projects associated with an employee when it is deleted.

- Package `seeders` contains all seeder classes. These are useful in testing phase. Finally, the Seeders class was created to facilitate the use of the individual Seeder classes during the tests.
  - Each seeder class implements two method:
    - `create()` that create a single instance of an object
    - `createMany(int n)` that iterates the `create()` method `n` times to create `n` instances.

## Tests

Through the `jUnit` and `assertj` library, test classes were developed to test the functionality of the application. 

In particular: 

- Test cases were created to control the operation of the Service classes, so it was not necessary to test the Repository classes because the Service classes wrap the Repository.
- Test cases were created to verify the proper functioning of the Seeder classes.





