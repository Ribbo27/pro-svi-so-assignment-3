package com.ribbo.orm;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import com.ribbo.orm.entities.City;
import com.ribbo.orm.entities.Employee;
import com.ribbo.orm.entities.Office;
import com.ribbo.orm.entities.Project;

public class DBManager {
	
	public static void dropDB() {
		
		EntityManager entityManager = Persistence.createEntityManagerFactory("company").createEntityManager();
		
		String query = "TRUNCATE ";
		
		entityManager.getTransaction().begin();
		entityManager.createNativeQuery(query + Employee.tableName).executeUpdate();
		entityManager.createNativeQuery(query + Office.tableName).executeUpdate();
		entityManager.createNativeQuery(query + Project.tableName).executeUpdate();
		entityManager.createNativeQuery(query + City.tableName).executeUpdate();
		entityManager.createNativeQuery(query + Employee.tableName + "_" + Project.tableName).executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
//	UTILIZZANDO service.delete(T entity) VIENE CANCELLATO IL RECORD, 
//	CON TRUNCATE O DELETE NON FUNZIONA, PERCHE'?
	public static void cleanTable(String tableName) {
		
		EntityManager entityManager = Persistence.createEntityManagerFactory("company").createEntityManager();
		
		String query = "DELETE FROM ";
		
		entityManager.getTransaction().begin();
		entityManager.createNativeQuery(query + tableName).executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
	}
}
