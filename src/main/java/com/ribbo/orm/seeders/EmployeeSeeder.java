package com.ribbo.orm.seeders;

import java.util.*;

import com.github.javafaker.Faker;
import com.ribbo.orm.entities.Employee;

public class EmployeeSeeder {

	private Faker faker;
	
	public EmployeeSeeder() {
		this.faker = new Faker();
	}
	
	public Employee create() {
		
		String firstname = faker.name().firstName();
		String lastname = faker.name().lastName();
		String hire = faker.date().birthday().toString();
		String street = faker.address().streetName();
		String buildingNumber = faker.address().buildingNumber();
		String zipCode = faker.address().zipCode();
		double salary = faker.number().randomDouble(2, 1000, 10000);
		
		Employee employee = new Employee(firstname, lastname, salary, hire, street, buildingNumber, zipCode);
		
		return employee;
	}
	
	public List<Employee> createMany(int n) {
		
		List<Employee> employees = new ArrayList<Employee>();
		
		for(int i = 0; i < n; i++) {
			employees.add(this.create());
		}
		
		return employees;
	}
}
