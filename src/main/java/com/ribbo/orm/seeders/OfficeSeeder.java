package com.ribbo.orm.seeders;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.ribbo.orm.entities.Office;

public class OfficeSeeder {

	private Faker faker;

	public OfficeSeeder() {
		this.faker = new Faker();
	}
	
	public Office create() {
		
		String phoneNumber = faker.phoneNumber().cellPhone().toString();
		String street = faker.address().streetName();
		String buildingNumber = faker.address().buildingNumber();
		String zipCode = faker.address().zipCode();
		
		Office office = new Office(phoneNumber, street, buildingNumber, zipCode);
		
		return office;
	}
	
	public List<Office> createMany(int n) {
		
		List<Office> offices = new ArrayList<Office>();
		
		for(int i = 0; i < n; i++) {
			offices.add(this.create());
		}
		
		return offices;
	}
	
}