package com.ribbo.orm.seeders;

import java.util.*;

import com.ribbo.orm.entities.*;

public class Seeders {

	private static EmployeeSeeder eSeeder;
	private static ProjectSeeder pSeeder;
	private static OfficeSeeder oSeeder;
	private static CitySeeder cSeeder;
	
	static {
		eSeeder = new EmployeeSeeder();
		pSeeder = new ProjectSeeder();
		oSeeder = new OfficeSeeder();
		cSeeder = new CitySeeder();
	}
	
	public Seeders() {}
	
	public static Employee employee() {
		return eSeeder.create();
	}
	
	public static List<Employee> employee(int n) {
		return eSeeder.createMany(n);
	}
	
	public static Project project() {
		return pSeeder.create();
	}
	
	public static List<Project> project(int n) {
		return pSeeder.createMany(n);
	}
	
	public static Office office() {
		return oSeeder.create();
	}
	
	public static List<Office> office(int n) {
		return oSeeder.createMany(n);
	}
	
	public static City city() {
		return cSeeder.create();
	}
	
	public static List<City> city(int n) {
		return cSeeder.createMany(n);
	}
	
}
