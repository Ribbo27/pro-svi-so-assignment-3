package com.ribbo.orm.seeders;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.ribbo.orm.entities.Address;

public class AddressSeeder {

	private Faker faker;
	
	public AddressSeeder() {
		this.faker = new Faker();
	}
	
	public Address create() {
		
		String street = faker.address().streetName();
		String buildingNumber = faker.address().buildingNumber();
		String zipCode = faker.address().zipCode();
		
		Address address = new Address(street, buildingNumber, zipCode);
		
		return address;
	}
	
	public List<Address> createMany(int n) {
		
		List<Address> addresses = new ArrayList<Address>();
		
		for(int i = 0; i < n; i++) {
			addresses.add(this.create());
		}
		
		return addresses;
	}
}
