package com.ribbo.orm.seeders;

import java.util.*;

import com.github.javafaker.Faker;
import com.ribbo.orm.entities.Project;

public class ProjectSeeder {

	private Faker faker;
	
	public ProjectSeeder() {
		this.faker = new Faker();
	}
	
	public Project create() {
		
		String name = faker.name().title();
		int budget = faker.number().numberBetween(1000, 100000);
		
		Project project = new Project(name, budget);
		
		return project;
	}
	
	public List<Project> createMany(int n) {
		
		List<Project> projects = new ArrayList<Project>();
		
		for(int i = 0; i < n; i++) {
			projects.add(this.create());
		}
		
		return projects;
	}
}
