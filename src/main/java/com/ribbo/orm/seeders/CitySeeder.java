package com.ribbo.orm.seeders;

import java.util.ArrayList;
import java.util.List;

import com.github.javafaker.Faker;
import com.ribbo.orm.entities.City;

public class CitySeeder {

	private Faker faker;
	
	public CitySeeder() {
		this.faker = new Faker();
	}
	
	public City create() {
		
		String name = faker.address().city();
		String country = faker.address().country();
		
		City city = new City(name, country);
		
		return city;
	}
	
	public List<City> createMany(int n) {
		
		List<City> cities = new ArrayList<City>();
		
		for(int i = 0; i < n; i++) {
			cities.add(this.create());
		}
		
		return cities;
	}
}
