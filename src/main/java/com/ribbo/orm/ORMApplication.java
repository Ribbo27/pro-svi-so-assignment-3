package com.ribbo.orm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.ribbo.orm.entities", "com.ribbo.orm.repositories", "com.ribbo.orm.seeders", "com.ribbo.orm.services"})
public class ORMApplication {
	
	public static void main(String[] args) {
		
		SpringApplication.run(ORMApplication.class, args);
		
//		EmployeeSeeder employeeSeeder = new EmployeeSeeder();
//		EmployeeService emService = new EmployeeService();
//		
//		Employee employee = employeeSeeder.create();
//
//		employee = emService.saveEmployee(employee);
//		
//		List<Employee> list = emService.getAll();
//		
//		if(list.size() == 1) {
//			System.out.println("Test complete!");
//		} else {
//			System.out.println("Test failed!");
//		}
	}

}

