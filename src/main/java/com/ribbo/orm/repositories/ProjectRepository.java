package com.ribbo.orm.repositories;

import java.util.List;

import com.ribbo.orm.entities.Project;

public class ProjectRepository extends Repository<Project, Long>{

	public ProjectRepository() {}
	
	@Override
	public Project create(Project entity) {
		getEntityManager().persist(entity);
		return entity;
	}

	@Override
	public Project get(Long id) {
		return getEntityManager().find(Project.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> getAll() {
		return getEntityManager().createQuery("from Project").getResultList();
	}

	@Override
	public Project update(Project entity) {
		getEntityManager().merge(entity);
		return entity;
	}

	@Override
	public void delete(Project entity) {
		getEntityManager().remove(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Project> whereLike(String field, String value) {
		return getEntityManager().createQuery("SELECT p FROM Project p WHERE p." + field + " LIKE :value").setParameter("value", value).getResultList();
	}
	
}
