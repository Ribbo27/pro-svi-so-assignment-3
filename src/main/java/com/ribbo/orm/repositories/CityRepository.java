package com.ribbo.orm.repositories;

import java.util.List;

import com.ribbo.orm.entities.City;

public class CityRepository extends Repository<City, Long>{

	public CityRepository() {}
	
	@Override
	public City create(City entity) {
		getEntityManager().persist(entity);
		return entity;
	}

	@Override
	public City get(Long id) {
		return getEntityManager().find(City.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<City> getAll() {
		return getEntityManager().createQuery("from City").getResultList();
	}

	@Override
	public City update(City entity) {
		return getEntityManager().merge(entity);
	}

	@Override
	public void delete(City entity) {
		getEntityManager().remove(entity);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<City> whereLike(String field, String value) {
		return getEntityManager().createQuery("SELECT c FROM City c WHERE c." + field + " LIKE :value").setParameter("value", value).getResultList();
	}



	

}
