package com.ribbo.orm.repositories;

import java.util.List;

import com.ribbo.orm.entities.Office;

public class OfficeRepository extends Repository<Office, Long>{

	@Override
	public Office create(Office entity) {
		getEntityManager().persist(entity);
		return entity;
	}

	@Override
	public Office get(Long id) {
		return getEntityManager().find(Office.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Office> getAll() {
		return getEntityManager().createQuery("from Office").getResultList();
	}

	@Override
	public Office update(Office entity) {
		return getEntityManager().merge(entity);
	}

	@Override
	public void delete(Office entity) {
		getEntityManager().remove(entity);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Office> whereLike(String field, String value) {
		return getEntityManager().createQuery("SELECT o FROM Office o WHERE o." + field + " LIKE :value").setParameter("value", value).getResultList();
	}
	
}
