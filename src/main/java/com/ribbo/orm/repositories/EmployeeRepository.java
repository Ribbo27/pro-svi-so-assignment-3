package com.ribbo.orm.repositories;

import java.util.List;

import com.ribbo.orm.entities.Employee;

public class EmployeeRepository extends Repository<Employee, Long>{
	
	public EmployeeRepository() {}
	
	@Override
	public Employee create(Employee entity) {
		getEntityManager().persist(entity);
		return entity;
	}

	@Override
	public Employee get(Long id) {
		return getEntityManager().find(Employee.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getAll() {
		return getEntityManager().createQuery("from Employee").getResultList();
	}
	
	@Override
	public Employee update(Employee entity) {
		return getEntityManager().merge(entity);
	}

	@Override
	public void delete(Employee entity) {
		getEntityManager().remove(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> whereLike(String field, String value) {
		return getEntityManager().createQuery("SELECT e FROM Employee e WHERE e." + field + " LIKE :value").setParameter("value", value).getResultList();
	}
	
}
