package com.ribbo.orm.services;

import java.io.Serializable;
import java.util.List;

public interface Services<T, id extends Serializable> {

	public T save(T t);
	public T get(Long id);
	public List<T> getAll();
	public T edit(T t);
	public void delete(T t);
	public void delete(Long id);
	public List<T> whereLike(String field, String value);
	
}
