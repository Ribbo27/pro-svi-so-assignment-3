package com.ribbo.orm.services;

import java.util.List;

import com.ribbo.orm.entities.City;
import com.ribbo.orm.repositories.CityRepository;

public class CityService implements Services<City, Long>{

	private CityRepository repo;
	
	public CityService() {
		this.repo = new CityRepository();
	}
	
	public City save(City city) {
		repo.open();
		repo.create(city);
		repo.close();
		return city;
	}

	public City get(Long id) {
		repo.open();
		City found = repo.get(id);
		repo.close();
		return found;
	}

	public List<City> getAll() {
		repo.open();
		List<City> founds = repo.getAll();
		repo.close();
		return founds;
	}

	public City edit(City city) {
		repo.open();
		City updated = repo.update(city);
		repo.close();
		return updated;
	}

	public void delete(City city) {
		repo.open();
		repo.delete(city);
		repo.close();
	}

	public void delete(Long id) {
		delete(repo.get(id));
	}
	
	public List<City> whereLike(String field, String value) {
		repo.open();
		List<City> founds = repo.whereLike(field, value);
		repo.close();
		return founds;
	}
	
}
