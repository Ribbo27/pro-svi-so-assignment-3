package com.ribbo.orm.services;

import java.util.List;

import com.ribbo.orm.entities.Office;
import com.ribbo.orm.repositories.OfficeRepository;

public class OfficeService implements Services<Office, Long>{

	private OfficeRepository repo;
	
	public OfficeService() {
		this.repo = new OfficeRepository();
	}

	public Office save(Office entity) {
		repo.open();
		Office of = repo.create(entity);
		repo.close();
		return of;
	}

	public Office get(Long id) {
		repo.open();
		Office found = repo.get(id);
		repo.close();
		return found;
	}

	public List<Office> getAll() {
		repo.open();
		List<Office> ofs = repo.getAll();
		repo.close();
		return ofs;
	}

	public Office edit(Office entity) {
		repo.open();
		Office merged = repo.update(entity);
		repo.close();
		return merged;
	}

	public void delete(Office entity) {
		repo.open();
		repo.delete(entity);
		repo.close();
	}

	public void delete(Long id) {
		Office toDelete = get(id);
		delete(toDelete);
	}

	public List<Office> whereLike(String field, String value) {
		repo.open();
		List<Office> founds = repo.whereLike(field, value);
		repo.close();
		return founds;
	}
	
	
}
