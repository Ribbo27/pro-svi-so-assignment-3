package com.ribbo.orm.services;

import java.util.List;

import com.ribbo.orm.entities.Project;
import com.ribbo.orm.repositories.ProjectRepository;

public class ProjectService implements Services<Project, Long> {

	private ProjectRepository repo;
	
	public ProjectService() {
		this.repo = new ProjectRepository();
	}
	
	public Project save(Project project) {
		repo.open();
		repo.create(project);
		repo.close();
		return project;
	}

	public Project get(Long id) {
		repo.open();
		Project found = repo.get(id);
		repo.close();
		return found;
	}

	public List<Project> getAll() {
		repo.open();
		List<Project> founds = repo.getAll();
		repo.close();
		return founds;
	}

	public Project edit(Project project) {
		repo.open();
		Project updated = repo.update(project);
		repo.close();
		return updated;
	}

	public void delete(Project project) {
		repo.open();
		repo.delete(project);
		repo.close();
	}

	public void delete(Long id) {
		delete(repo.get(id));		
	}

	public List<Project> whereLike(String field, String value) {
		repo.open();
		List<Project> founds = repo.whereLike(field, value);
		repo.close();
		return founds;
	}

}
