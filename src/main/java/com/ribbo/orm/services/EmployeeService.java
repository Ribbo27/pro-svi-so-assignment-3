package com.ribbo.orm.services;

import java.util.List;

import com.ribbo.orm.entities.Employee;
import com.ribbo.orm.repositories.EmployeeRepository;

public class EmployeeService implements Services<Employee, Long>{

	private EmployeeRepository repo;
	
	public EmployeeService() {
		this.repo = new EmployeeRepository();
	}
	
	public Employee save(Employee employee) {
		repo.open();
		Employee em = repo.create(employee);
		repo.close();
		return em;
	}

	public Employee get(Long id) {
		repo.open();
		Employee found = repo.get(id);
		repo.close();
		return found;
	}

	public List<Employee> getAll() {
		repo.open();
		List<Employee> emps = repo.getAll();
		repo.close();
		return emps;
	}

	public Employee edit(Employee toUpdate) {
		repo.open();
		Employee updated = repo.update(toUpdate);
		repo.close();
		return updated;
	}

	public void delete(Employee toDelete) {
		repo.open();
		toDelete.remove();
		repo.delete(toDelete);
		repo.close();
	}

	public void delete(Long id) {
		Employee toDelete = get(id);
		delete(toDelete);
	}

	public List<Employee> whereLike(String field, String value) {
		repo.open();
		List<Employee> founds = repo.whereLike(field, value);
		repo.close();
		return founds;
	}

}
