package com.ribbo.orm.entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name=Employee.tableName)
public class Employee
{
	public static final String tableName = "Employees";
	
	@Id
	@Column(name="EMPLOYEE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
    private long employeeId;
	@Column(name="FIRSTNAME")
    private String firstname;
	@Column(name="LASTNAME")
    private String lastname;
	@Column(name="SALARY")
	private double salary;
	@Column(name="HIRE_DATE")
	private String hire;
	@Embedded
	private Address address;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})	
	@JoinColumn(name="CITY_ID", referencedColumnName="CITY_ID")
	private City city;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})	
	@JoinColumn(name="OFFICE_ID", referencedColumnName="OFFICE_ID")
	private Office office;
	
	@ManyToMany(cascade = {CascadeType.PERSIST,	CascadeType.MERGE}, fetch = FetchType.LAZY)
	@JoinTable(name = "Employees_Projects",
		joinColumns = @JoinColumn(name = "EMPLOYEE_ID", referencedColumnName="EMPLOYEE_ID"),
		inverseJoinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName="PROJECT_ID")
	)
	private List<Project> projects;

	public Employee() {}
	
	public Employee(String firstname, String lastname, double salary, String hire, String street, String number, String zipCode) {	
		this.firstname = firstname;
		this.lastname = lastname;
		this.hire = hire;
		this.salary = salary;
		this.address = new Address(street, number, zipCode);
		this.projects = new ArrayList<Project>();
		
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public String getHire() {
		return hire;
	}

	public void setHire(String hire) {
		this.hire = hire;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public void addProject(Project project) {
		this.projects.add(project);
		project.getEmployees().add(this);
	}
	
	public void addProjects(List<Project> projects) {
		this.projects.addAll(projects);
		for(Project p: projects) {
			p.getEmployees().add(this);
		}
	}
	
	public void removeProject(Project project) {
		this.projects.remove(project);
		project.getEmployees().remove(this);
	}
	
	public void remove() {
		for(Project p: new ArrayList<Project>(projects)) {
			removeProject(p);
		}
	}
	
	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", salary=" + salary + ", hire=" + hire + ", address=" + address.toString() + ", office=" + office.toString()
				+ ", projects=" + projects.toString() + "]";
	}
		
}