package com.ribbo.orm.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Address {
	
	@Column(name="STREET")
	private String street;
	@Column(name="NUMBER")
	private String number;
	@Column(name="ZIP_CODE")
	private String zipCode;
	
	public Address() {}
	
	public Address(String street, String number, String zipCode) {
		this.street = street;
		this.number = number;
		this.zipCode = zipCode;
	}
	
	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public String toString() {
		return "Address [street=" + street + ", number=" + number + ", zipCode="
				+ zipCode + "]";
	}
	

}
