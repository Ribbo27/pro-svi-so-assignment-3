package com.ribbo.orm.entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name=Project.tableName)
public class Project {

	public static final String tableName = "Projects";
	
	@Id
	@Column(name="PROJECT_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long projectId;
	@Column(name="NAME", unique = true)
	private String name;
	@Column(name="BUDGET")
	private int budget;

	@OneToOne()
	@JoinColumn(name="PARENT_ID")
	private Project parent_project;
	@OneToMany(mappedBy="parent_project", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Project> subProjects;
	
	@ManyToMany(mappedBy="projects", cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
	private List<Employee> employees;

	public Project() {}
	
	public Project(String name, int budget) {
		super();
		this.name = name;
		this.budget = budget;
		this.employees = new ArrayList<Employee>();
		this.subProjects = new ArrayList<Project>();
	}
	
	public long getProjectId() {
		return projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBudget() {
		return budget;
	}

	public void setBudget(int budget) {
		this.budget = budget;
	}

	public Project getParent_project() {
		return parent_project;
	}

	public void setParent_project(Project parent_project) {
		this.parent_project = parent_project;
	}

	public List<Project> getSubProjects() {
		return subProjects;
	}

	public void setSubProjects(List<Project> subProjects) {
		this.subProjects = subProjects;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

//	@Override
//	public String toString() {
//		return "Project [projectId=" + projectId + ", name=" + name + ", budget=" + budget + ", parent_project="
//				+ parent_project + ", subProjects=" + subProjects + ", employees=" + employees.toString() + "]";
//	}	
	
}
