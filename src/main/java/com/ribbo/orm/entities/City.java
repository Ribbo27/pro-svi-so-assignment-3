package com.ribbo.orm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name=City.tableName)
public class City {

	public static final String tableName = "Cities";
	
	@Id
	@Column(name="CITY_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long cityId;
	@Column(name="NAME", unique = true) 
	private String name;
	@Column(name="COUNTRY")
	private String country;
	
	public City() {}
	
	public City(String name, String country) {
		this.name = name;
		this.country = country;
	}
	
	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

//	public Set<Office> getOffices() {
//		return offices;
//	}
//
//	public void setOffices(Set<Office> offices) {
//		this.offices = offices;
//	}

	@Override
	public String toString() {
		return "City [cityId=" + cityId + ", name=" + name + ", country=" + country
				+ "]";
	}
	
}
