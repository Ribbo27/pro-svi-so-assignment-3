package com.ribbo.orm.entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name=Office.tableName)
public class Office {
	
	public static final String tableName = "Offices";
	
	@Id
	@Column(name = "OFFICE_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long officeId;
	@Column(name = "PHONE_NUMBER")
	private String phoneNumber;
	@Embedded
	private Address address;
	
	//Relationship
	@OneToMany(mappedBy="office", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Employee> employees;
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="CITY_ID", referencedColumnName="CITY_ID")
	private City city;
	
	public Office() {}
	
	public Office(String phoneNumber, String street, String buildingNumber, String zipCode ) {
		this.phoneNumber = phoneNumber;
		this.address = new Address(street, buildingNumber, zipCode);
		this.employees = new ArrayList<Employee>();
	}
	
	
	public long getOfficeId() {
		return officeId;
	}

	public void setOfficeId(long officeId) {
		this.officeId = officeId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Office [officeId=" + officeId + ", phoneNumber=" + phoneNumber + ", address=" + address.toString()
				 + ", city=" + city.toString() + "]";
	}

}
