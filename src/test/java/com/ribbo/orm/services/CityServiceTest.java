package com.ribbo.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;
import com.ribbo.orm.seeders.Seeders;
import com.ribbo.orm.DBManager;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CityServiceTest {
	
	private CityService service;
	private List<City> cities;
	
	public CityServiceTest() {
		this.service = new CityService();
		this.cities = Seeders.city(5);
	}
	
	public void populateDB() {
		
		for(City c : cities) {
			service.save(c);
		}
		
	}
	
	@Before
	public void setUp() {
		DBManager.dropDB();
		assertThat(service).isNotNull();
		populateDB();
	}
	
	@Test
	public void testAddACity() {
		
		City city = cities.get(0);
		
		service.save(city);
		
		assertThat(service.get(cities.get(0).getCityId())).hasFieldOrPropertyWithValue("name", cities.get(0).getName());
		
	}
	
	@Test
	public void testUpdateACity() {
		
		City toUpdate = cities.get(3);
		
		toUpdate.setName("Milano");
		
		service.edit(toUpdate);
		
		assertThat(service.get(toUpdate.getCityId()).getName()).isEqualTo("Milano");
		
	}
	
	@Test
	public void testRemoveACityByEntity() {
		
		City toDelete = cities.get(4);
		
		service.delete(toDelete);
		
		assertThat(service.getAll()).hasSize(4);
		
	}
	
	@Test
	public void testRemoveACityByID() {
		
		City toDelete = cities.get(4);
		
		service.delete(toDelete.getCityId());
		
		assertThat(service.getAll()).hasSize(4);
		
	}
	
	@Test
	public void testWhereLike() {
		
		cities.get(0).setCountry("Italy");
		cities.get(1).setCountry("Italy");
		cities.get(4).setCountry("Italy");
		
		for(City c : cities) {
			service.edit(c);
		}
		
		assertThat(service.whereLike("country", "Italy")).hasSize(3);
		
	}
	
	@AfterClass
	public static void cleanDB() {
		DBManager.dropDB();
	}
}
