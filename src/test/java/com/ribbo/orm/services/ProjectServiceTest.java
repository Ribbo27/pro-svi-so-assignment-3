package com.ribbo.orm.services;


import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;
import com.ribbo.orm.DBManager;
import com.ribbo.orm.seeders.Seeders;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProjectServiceTest {
 
	private ProjectService pService;
	private EmployeeService eService;
	List<Project> projects;
	List<Employee> parent_emps;	
	List<Employee> sub0_emps; 
	List<Employee> sub1_emps; 
	
	
	public ProjectServiceTest() {
		this.pService = new ProjectService();
		this.eService = new EmployeeService();
		this.projects = Seeders.project(8);
		this.parent_emps = Seeders.employee(4);
		this.sub0_emps = Seeders.employee(2);
		this.sub1_emps = Seeders.employee(2);
	}
	
	public void populateDB() {
		
		for(Project p: projects) {
			pService.save(p);
		}
		
	}
	
	@Before
	public void setUp() {
		DBManager.dropDB();
		assertThat(pService).isNotNull();
		assertThat(eService).isNotNull();
		populateDB();
	}
	
	@Test
	public void testAddAProject() {
		
		Project project = projects.get(0);
				
		pService.save(project);
		
		assertThat(pService.get(projects.get(0).getProjectId())).hasFieldOrPropertyWithValue("name", projects.get(0).getName());
		
	}
	
	@Test
	public void testAddAProjectWithEntitiesAssociation() {
		
		Project parent = projects.get(0);
		List<Project> subs = new ArrayList<Project>();
		
		subs.add(projects.get(1));
		subs.add(projects.get(2));
		
		parent.setSubProjects(subs);
		parent.setEmployees(parent_emps);
		subs.get(0).setParent_project(parent);
		subs.get(0).setEmployees(sub0_emps);
		subs.get(1).setParent_project(parent);
		subs.get(1).setEmployees(sub1_emps);
		
		pService.save(parent);
		
		assertThat(pService.getAll()).hasSize(8);		
		assertThat(pService.get(parent.getProjectId()).getEmployees()).hasSize(4);
		assertThat(pService.get(parent.getProjectId()).getSubProjects()).hasSize(2);
		assertThat(pService.get(parent.getProjectId()).getSubProjects().get(0).getEmployees()).hasSize(2);
		assertThat(pService.get(parent.getProjectId()).getSubProjects().get(1).getEmployees()).hasSize(2);
	}
	
	@Test 
	public void testUpdateAProject() {
		
		Project toUpdate = pService.get(projects.get(0).getProjectId());
		
		toUpdate.setName("ProSviSoAssignment3");
		
		pService.edit(toUpdate);
		
		assertThat(pService.get(toUpdate.getProjectId()).getName()).isEqualTo("ProSviSoAssignment3");

	}
	
	@Test
	public void testUpdateAProjectWithEntitiesAssociation() {
		
		Project toUpdate = pService.get(projects.get(0).getProjectId());
		
		toUpdate.getSubProjects().add(projects.get(1));
		toUpdate.getSubProjects().add(projects.get(2));
		toUpdate.setEmployees(parent_emps);
		toUpdate.getSubProjects().get(0).setParent_project(toUpdate);
		toUpdate.getSubProjects().get(0).setEmployees(sub0_emps);
		toUpdate.getSubProjects().get(1).setParent_project(toUpdate);
		toUpdate.getSubProjects().get(1).setEmployees(sub1_emps);
		
		pService.edit(toUpdate);
		
		assertThat(pService.getAll()).hasSize(8);
		assertThat(pService.get(toUpdate.getProjectId()).getEmployees()).hasSize(4);
		assertThat(pService.get(toUpdate.getProjectId()).getSubProjects()).hasSize(2);
	}

	@Test
	public void testRemoveAProjectByEntity() {
		
		Project toDelete = pService.get(projects.get(7).getProjectId());	
		
		pService.delete(toDelete);
		
		assertThat(pService.getAll()).hasSize(7);
	}
	
	@Test
	public void testRemoveAProjectByID() {
		
		Project toDelete = pService.get(projects.get(5).getProjectId());
		
		pService.delete(toDelete.getProjectId());
		
		assertThat(pService.getAll()).hasSize(7);
	}
	
	@Test
	public void testRemoveAProjectByEntityWithEntitiesAssociation() {
		
		Project toDelete = pService.get(projects.get(6).getProjectId());
		
		toDelete.setEmployees(sub0_emps);
		toDelete.setParent_project(projects.get(5));
		
		pService.edit(toDelete);
		pService.delete(toDelete);
		
		assertThat(pService.getAll()).hasSize(7);
		assertThat(eService.get(sub0_emps.get(0).getEmployeeId()).getProjects()).doesNotContain(toDelete);
		
	}
	
	@Test
	public void testWhereLike() {
		
		projects.get(0).setName("ProSviSo0");
		projects.get(1).setName("ProSviSo1");
		projects.get(2).setName("ProSviSo2");
		
		for(Project p: projects) {
			pService.edit(p);
		}
		
		assertThat(pService.whereLike("name", "ProSviSo0").get(0).getName()).isEqualTo(projects.get(0).getName());
		assertThat(pService.whereLike("name", "ProSviSo1").get(0).getName()).isEqualTo(projects.get(1).getName());
		assertThat(pService.whereLike("name", "ProSviSo2").get(0).getName()).isEqualTo(projects.get(2).getName());
		
	}
	
	@AfterClass
	public static void cleanDB() {
		DBManager.dropDB();
	}
	
	
}
