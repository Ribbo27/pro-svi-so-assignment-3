package com.ribbo.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;
import com.ribbo.orm.DBManager;
import com.ribbo.orm.seeders.Seeders;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeServiceTest {

	private EmployeeService eService; 
	private ProjectService pService;
	private CityService cService; 
	private OfficeService oService;
	List<Employee> emps;
	List<Project> projects;
	List<Office> offices;
	List<City> cities;

	public EmployeeServiceTest() {
		this.eService = new EmployeeService();
		this.cService = new CityService();
		this.oService = new OfficeService();
		this.pService = new ProjectService();
		this.emps = Seeders.employee(10);
		this.projects = Seeders.project(4);
		this.offices = Seeders.office(2);
		this.cities = Seeders.city(5);
	}
	
	public void populateDB() {

		for(Employee e: emps) {
			eService.save(e);
		}
		
	}
	
	@Before
	public void setUp() {
		DBManager.dropDB();
		assertThat(eService).isNotNull();
		assertThat(cService).isNotNull();
		assertThat(oService).isNotNull();
		assertThat(pService).isNotNull();
		populateDB();
	}
	
	@Test
	public void testAddAnEmployee() {
		
		Employee employee = emps.get(0);
		
		eService.save(employee);
		
		assertThat(eService.get(emps.get(0).getEmployeeId())).hasFieldOrPropertyWithValue("firstname", emps.get(0).getFirstname());
		
	}
	
	@Test
	public void testAddAnEmployeeWithEntitiesAssociation() {
		
		Employee employee = emps.get(0);
		Office office = offices.get(0);
		City city = cities.get(0);
		
		employee.setCity(city);
		employee.setOffice(office);
		employee.setProjects(projects);
		
		eService.save(employee);
		
		assertThat(eService.get(employee.getEmployeeId()).getProjects()).hasSize(4);
		assertThat(eService.get(employee.getEmployeeId())).hasFieldOrPropertyWithValue("office", offices.get(0));
		assertThat(eService.get(employee.getEmployeeId())).hasFieldOrPropertyWithValue("city", cities.get(0));

	}

	@Test
	public void testUpdateAnEmployee() {
		
		Employee toUpdate = eService.get(emps.get(0).getEmployeeId());
		
		toUpdate.setFirstname("Mario");
		
		eService.edit(toUpdate);
		
		assertThat(toUpdate.getFirstname()).isEqualTo(eService.get(emps.get(0).getEmployeeId()).getFirstname());
		
	}
	
	@Test
	public void testUpdateAnEmployeeWithEntitiesAssociation() {
		
		Employee toUpdate = eService.get(emps.get(2).getEmployeeId());
		
		toUpdate.setCity(cities.get(4));
		toUpdate.setOffice(offices.get(0));
		
		eService.edit(toUpdate);
				
		assertThat(eService.get(toUpdate.getEmployeeId()).getCity()).isEqualTo(cService.whereLike("name", cities.get(4).getName()).get(0));
		assertThat(eService.get(toUpdate.getEmployeeId()).getOffice()).isEqualTo( oService.whereLike("phoneNumber", offices.get(0).getPhoneNumber()).get(0));
		
	}

	@Test
	public void testRemoveAnEmployeeByEntity() {
		
		Employee toDelete = eService.get(emps.get(0).getEmployeeId());
		
		eService.delete(toDelete);
		
		assertThat(eService.getAll()).hasSize(9);

	}
	
	@Test
	public void testRemoveAnEmployeeByID() {
		
		Employee toDelete = eService.get(emps.get(0).getEmployeeId());
		
		eService.delete(toDelete.getEmployeeId());
		
		assertThat(eService.getAll()).hasSize(9);

	}
	
	@Test
	public void testRemoveAnEmployeeByEntityWithEntitiesAssociation() {
		
		Employee toDelete = emps.get(0);
		Office office = offices.get(0);
		City city = cities.get(0);
		
		toDelete.setCity(city);
		toDelete.setOffice(office);
		toDelete.addProjects(projects);
		
		eService.edit(toDelete);
		eService.delete(toDelete);
		
		assertThat(eService.getAll()).hasSize(9);
		assertThat(pService.getAll()).hasSize(4);
		
	}
	
	@Test
	public void testWhereLike() {
				
		emps.get(0).setFirstname("Luca");
		emps.get(4).setFirstname("Luca");
		emps.get(7).setFirstname("Luca");
		
		for(Employee e: emps) {
			eService.edit(e);
		}
		
		assertThat(eService.whereLike("firstname", "Luca")).hasSize(3);
	}
	
	@AfterClass
	public static void cleanDB() {
		DBManager.dropDB();
	}
	
}
