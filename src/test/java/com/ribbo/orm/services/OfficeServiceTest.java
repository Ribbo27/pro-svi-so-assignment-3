package com.ribbo.orm.services;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;
import com.ribbo.orm.seeders.Seeders;
import com.ribbo.orm.services.OfficeService;
import com.ribbo.orm.DBManager;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfficeServiceTest {

	private OfficeService oService;
	private EmployeeService eService;
	private CityService cService;
	private List<Office> offices;
	private List<Employee> emps;
	private List<City> cities;
	
	public OfficeServiceTest() {
		this.oService = new OfficeService();
		this.eService = new EmployeeService();
		this.cService = new CityService();
		this.offices = Seeders.office(5);
		this.emps = Seeders.employee(10);
		this.cities = Seeders.city(5);
	}
	
	public void populateDB() {
		
		for(Office o: offices) {
			oService.save(o);
		}
		
	}
	
	@Before
	public void setUp() {
		DBManager.dropDB();
		assertThat(oService).isNotNull();
		assertThat(eService).isNotNull();
		populateDB();
	}
	
	@Test
	public void testAddAnOffice() {
		
		Office office = offices.get(0);
		
		oService.save(office);
		
		assertThat(oService.get(office.getOfficeId())).hasFieldOrPropertyWithValue("phoneNumber", offices.get(0).getPhoneNumber());
		
	}
	
	@Test
	public void testAddAnOfficeWithEntitiesAssociation() {
		
		Office office = offices.get(3);
		
		office.setCity(cities.get(3));
		office.setEmployees(emps);
		
		oService.save(office);
		
		assertThat(eService.getAll()).hasSize(10);
		assertThat(cService.getAll().get(0)).hasFieldOrPropertyWithValue("name", cities.get(3).getName());
		
	}
	
	@Test
	public void testUpdateAnOffice() {
		
		Office office = oService.get(offices.get(4).getOfficeId());
		
		office.setPhoneNumber("02957846352");
		
		oService.edit(office);
		
		assertThat(oService.get(office.getOfficeId()).getPhoneNumber()).isEqualTo("02957846352");
		
	}
	
	@Test
	public void testUpdateAnOfficeWithEntitiesAssociation() {
		
		Office toUpdate = oService.get(offices.get(2).getOfficeId());
		
		toUpdate.setEmployees(emps);
		toUpdate.setCity(cities.get(0));
		toUpdate.setPhoneNumber("1234567890");
		
		oService.edit(toUpdate);
	
		assertThat(oService.get(toUpdate.getOfficeId()).getEmployees()).hasSize(10);
		assertThat(oService.get(toUpdate.getOfficeId()).getCity()).isEqualTo(cService.whereLike("name", cities.get(0).getName()).get(0));
		assertThat(oService.get(toUpdate.getOfficeId())).hasFieldOrPropertyWithValue("phoneNumber", "1234567890");
		
	}
	
	@Test
	public void testRemoveAnOfficeByEntity() {
		
		Office toDelete = oService.get(offices.get(4).getOfficeId());
		
		oService.delete(toDelete);
		
		assertThat(oService.getAll()).hasSize(4);
		
	}
	
	@Test
	public void testRemoveAnOfficeByID() {
		
		Office toDelete = oService.get(offices.get(4).getOfficeId());
		
		oService.delete(toDelete.getOfficeId());
		
		assertThat(oService.getAll()).hasSize(4);
		
	}
	
	@Test
	public void testRemoveAnOfficeByEntityWithAssociation() {
		
		Office toDelete = oService.get(offices.get(4).getOfficeId());
		
		toDelete.setCity(cities.get(3));
		toDelete.setEmployees(emps);
		
		oService.delete(toDelete.getOfficeId());
		
		assertThat(oService.getAll()).hasSize(4);
		assertThat(eService.getAll()).hasSize(10);
		assertThat(cService.getAll().get(0)).isEqualTo(cService.whereLike("name", cities.get(3).getName()).get(0));
		
	}
	
	@Test
	public void testWhereLike() {
		
		assertThat(oService.whereLike("phoneNumber", offices.get(1).getPhoneNumber()).get(0)).isEqualTo(offices.get(1));
		
	}
	
	@AfterClass
	public static void cleanDB() {
		DBManager.dropDB();
	}
	
}
