package com.ribbo.orm.seeders;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;

@RunWith(SpringRunner.class)
@DataJpaTest

public class ProjectSeederTest {
	
	private ProjectSeeder seeder;
	
	public ProjectSeederTest() {
		this.seeder = new ProjectSeeder();
	}
	
	@Test
	public void testCreate() {
		
		Project toCreate = seeder.create();
		
		assertThat(toCreate).hasFieldOrProperty("name");
		assertThat(toCreate).hasFieldOrProperty("budget");
		
	}
	
	@Test
	public void testCreateMany() {
		
		List<Project> toCreate = seeder.createMany(5);
		
		assertThat(toCreate).hasSize(5);
		
	}
	
}
