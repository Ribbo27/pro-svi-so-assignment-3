package com.ribbo.orm.seeders;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;

@RunWith(SpringRunner.class)
@DataJpaTest

public class SeedersTest {
	
	public SeedersTest() {}
	
	@Test
	public void testEmployee() {
		
		Employee e = Seeders.employee();
		
		assertThat(e).isExactlyInstanceOf(Employee.class);
		
	}
	
	@Test
	public void testSomeEmployee() {
		
		List<Employee> emps = Seeders.employee(3);
		
		for(Employee e : emps) {
			assertThat(e).isExactlyInstanceOf(Employee.class);
		}
		
	}
	
	@Test
	public void testProject() {
		
		Project p = Seeders.project();
		
		assertThat(p).isExactlyInstanceOf(Project.class);
		
	}
	
	@Test
	public void testSomeProject() {
		
		List<Project> projects = Seeders.project(3);
		
		for(Project p : projects) {
			assertThat(p).isExactlyInstanceOf(Project.class);
		}
		
	}
	
	@Test
	public void testOffice() {
		
		Office o = Seeders.office();
		
		assertThat(o).isExactlyInstanceOf(Office.class);
		
	}
	
	@Test
	public void testSomeOffice() {
		
		List<Office> offices = Seeders.office(3);
		
		for(Office o : offices) {
			assertThat(o).isExactlyInstanceOf(Office.class);
		}
		
	}
	
	@Test
	public void testCity() {
		
		City c = Seeders.city();
		
		assertThat(c).isExactlyInstanceOf(City.class);
		
	}
	
	@Test
	public void testSomeCity() {
		
		List<City> cities = Seeders.city(3);
		
		for(City c : cities) {
			assertThat(c).isExactlyInstanceOf(City.class);
		}
		
	}

	
}
