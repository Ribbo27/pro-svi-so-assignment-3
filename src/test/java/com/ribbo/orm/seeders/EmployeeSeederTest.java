package com.ribbo.orm.seeders;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeSeederTest {
	
	private EmployeeSeeder seeder;
	
	public EmployeeSeederTest() {
		this.seeder = new EmployeeSeeder();
	}
	
	@Test
	public void testCreate() {
		
		Employee toCreate = seeder.create();
		
		assertThat(toCreate).hasFieldOrProperty("firstname");
		assertThat(toCreate).hasFieldOrProperty("lastname");
		assertThat(toCreate).hasFieldOrProperty("salary");
		assertThat(toCreate).hasFieldOrProperty("hire");
		assertThat(toCreate).hasFieldOrProperty("address");
		
	}
	
	@Test
	public void testCreateMany() {
		
		List<Employee> toCreate = seeder.createMany(5);
		
		assertThat(toCreate).hasSize(5);
		
	}
	
}
