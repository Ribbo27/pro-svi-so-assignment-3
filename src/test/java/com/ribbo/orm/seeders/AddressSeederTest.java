package com.ribbo.orm.seeders;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;

@RunWith(SpringRunner.class)
@DataJpaTest

public class AddressSeederTest {
	
	private AddressSeeder seeder;
	
	public AddressSeederTest() {
		this.seeder = new AddressSeeder();
	}
	
	@Test
	public void testCreate() {
		
		Address toCreate = seeder.create();
		
		assertThat(toCreate).hasFieldOrProperty("street");
		assertThat(toCreate).hasFieldOrProperty("number");
		assertThat(toCreate).hasFieldOrProperty("zipCode");
		
	}
	
	@Test
	public void testCreateMany() {
		
		List<Address> toCreate = seeder.createMany(5);
		
		assertThat(toCreate).hasSize(5);
		
	}
	
}
