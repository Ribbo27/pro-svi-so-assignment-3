package com.ribbo.orm.seeders;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;

@RunWith(SpringRunner.class)
@DataJpaTest

public class CitySeederTest {
	
	private CitySeeder seeder;
	
	public CitySeederTest() {
		this.seeder = new CitySeeder();
	}
	
	@Test
	public void testCreate() {
		
		City toCreate = seeder.create();
		
		assertThat(toCreate).hasFieldOrProperty("name");
		assertThat(toCreate).hasFieldOrProperty("country");
		
	}
	
	@Test
	public void testCreateMany() {
		
		List<City> toCreate = seeder.createMany(5);
		
		assertThat(toCreate).hasSize(5);
		
	}
	
}
