package com.ribbo.orm.seeders;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ribbo.orm.entities.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OfficeSeederTest {
	
	private OfficeSeeder seeder;
	
	public OfficeSeederTest() {
		this.seeder = new OfficeSeeder();
	}
	
	@Test
	public void testCreate() {
		
		Office toCreate = seeder.create();
		
		assertThat(toCreate).hasFieldOrProperty("phoneNumber");
		assertThat(toCreate).hasFieldOrProperty("address");
		
	}
	
	@Test
	public void testCreateMany() {
		
		List<Office> toCreate = seeder.createMany(5);
		
		assertThat(toCreate).hasSize(5);
		
	}
	
}
