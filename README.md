# Assignment 3 - ORM

### Student 

Francesco Ribaudo 807847

The whole project source code is available on: https://gitlab.com/Ribbo27/pro-svi-so-assignment-3.git 

The full report is available on: https://gitlab.com/Ribbo27/pro-svi-so-assignment-3/blob/master/REPORT.md

## How to use 

You can use the code through Docker-compose or using Eclipse.

### Docker-compose

- Clone the project from git to your pc
- Open a terminal and go to the project folder
- In the project folder, run `docker-compose build`
- After waiting for build command, run `docker-compose up`
- That's it, now you should see the tests performed and the results in your terminal

### Eclipse

- Run MySQL in a container:

  To do this run this command to create a MySQL container with the right environment variables:

​	`docker run -p 3306:3306 --name orm-mysql-prosviso3 -e MYSQL_ROOT_PASSWORD=secretpw -e MYSQL_DATABASE=companyDB -e MYSQL_USER=prosviso -e MYSQL_PASSWORD=prosvisopw -d mysql:5.7`

- Change database host in `persistence.xml` configuration file:

  Before running tests through Eclipse you have to change database host in `Persistence.xml` config file from `mysql` to `localhost`. 

- Import the project into Eclipse and run it:

  After importing the project, from Eclipse, right click on the `src/test/java` package, then click on `Run as` and then click on `JUnit Test`

- If you want to check database tables you have to connect to your MySQL container, to do this run:

  `docker run -it --link orm-mysql-prosviso3:mysql --rm mysql sh -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uprosviso -pprosvisopw'`





